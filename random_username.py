import sys, os, random

def strp_num(string):
    string = string.translate(None, '0123456789')
    return string

def mk_leet(string):
    replacements = (('a','4'),('e','3'),('i','1'), ('o','0'), ('t','7'))
    for old,new in replacements:
        string = string.replace(old,new)
    return string

def ran():
    string1 = strp_num(str.lower(random.choice(open('wordlist.txt').read().split()).strip()))
    string2 = strp_num(str.lower(random.choice(open('wordlist.txt').read().split()).strip()))
    string3 = string1 + '_' + string2
    return string3

print mk_leet(ran()) + '\n' + mk_leet(ran()) + '\n' + mk_leet(ran()) + '\n' + mk_leet(ran()) + '\n' + mk_leet(ran())
